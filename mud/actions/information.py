from .action import Action1,Action2
from mud.events import InformationEvent,InformationOnEvent

class InformationAction(Action1):
    EVENT = InformationEvent
    ACTION = "information"


class InformationOnAction(Action2):
    EVENT = InformationOnEvent
    RESOLVE_OBJECT = "resolve_for_operate"
    ACTION = "informationon"
