from .action import Action2,Action3
from mud.events import AttackEvent,AttackWithEvent

class AttackAction(Action2):
    EVENT = AttackEvent
    RESOLVE_OBJECT = "resolve_for_operate"
    ACTION = "attack"

class AttackWithAction(Action3):
    EVENT = AttackWithEvent
    ACTION = "attack-with"
    RESOLVE_OBJECT = "resolve_for_operate"
    RESOLVE_OBJECT2 = "resolve_for_use"