# -*- coding: utf-8 -*-
#==============================================================================

from .action import Action2
from mud.events import ConsumEvent

class ConsumAction(Action2):
    EVENT = ConsumEvent
    ACTION = "drop"
    RESOLVE_OBJECT = "resolve_for_use"
