# -*- coding: utf-8 -*-
# Copyright (C) 2014 Denys Duchier, IUT d'Orléans
#==============================================================================

from .model import Model
from .mixins.located import Located

class Thing(Located, Model):

    """a Thing is located in the world."""

    #--------------------------------------------------------------------------
    # initialization
    #--------------------------------------------------------------------------

    def __init__(self, **kargs):
        super().__init__(**kargs)

    #--------------------------------------------------------------------------
    # initialization from YAML data
    #--------------------------------------------------------------------------

    def init_from_yaml(self, data, world):
        super().init_from_yaml(data, world)

    def update_from_yaml(self, data, world):
        super().update_from_yaml(data, world)

    #--------------------------------------------------------------------------
    # API for saving the dynamic part of objects to YAML (via JSON)
    #--------------------------------------------------------------------------

    def archive_into(self, obj):
        super().archive_into(obj)

    def get_heal(self):
        for prop in self.get_props():
            if prop[:3] == "hp=":
                return int(prop[3:])
        return 0

    def get_dmg(self):
        typeArme = {"fire":2,"dark":50,"light":1,"ice":1,"earth":1,"water":1,"thunder":6,"wind":1,"heal":1,"divin":8998}

        for prop in self.get_props():
            if prop[:4]=="dmg=":
                if "divin" in self.get_props():
                    return int(prop[4:])+typeArme["divin"]

                for prop2 in self.get_props():
                    if prop2 in typeArme:
                        return int(prop[4:])+typeArme[prop2]
                return int(prop[4:] )
        return 0

    def add_effet(self, object2):
        typeEffet = {"light-on","fire","light","ice","earth","water","thunder","darkT","wind","heal","divin"}
        for prop in object2.get_props():
            if prop in typeEffet:
                if prop == "light":
                    self.add_prop("light-on") # pour que la lumiere nous laisse voir dans l'ombre
                else :
                    for elem in self.get_props():
                        if elem in typeEffet :
                            self.remove_prop(elem)
                    self.add_prop(prop)
                for dura in self.get_props():
                    if dura[:4] == "sta=" : # l'arme ne peut plus être utiliser si la durabilité est à 0
                        if int(dura[4:])>0:
                            self.yaml[dura]= "sta="+str(int(dura[4:])-1)
                        else :
                            self.remove_prop("weapon")
                return self.name
