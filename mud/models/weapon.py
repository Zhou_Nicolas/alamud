from .model import Model
from .mixins.located import Located


class Weapon(Located, Model):
    """a Weapon is located in the world."""

    # --------------------------------------------------------------------------
    # initialization
    # --------------------------------------------------------------------------

    def __init__(self, **kargs):
        super().__init__(**kargs)

    # --------------------------------------------------------------------------
    # initialization from YAML data
    # --------------------------------------------------------------------------

    def init_from_yaml(self, data, world):
        super().init_from_yaml(data, world)

    def update_from_yaml(self, data, world):
        super().update_from_yaml(data, world)

    # --------------------------------------------------------------------------
    # API for saving the dynamic part of objects to YAML (via JSON)
    # --------------------------------------------------------------------------

    def archive_into(self, obj):
        super().archive_into(obj)

    def get_dmg(self):
        for prop in self.get_props():
            if prop[:4] == "dmg=":
                return int(prop[4:])
