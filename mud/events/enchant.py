# -*- coding: utf-8 -*-
# Copyright (C) 2014 Denys Duchier, IUT d'Orléans
#==============================================================================


from .event import Event3

class EnchantWithEvent(Event3):
    NAME = "enchant-with"

    def perform(self):
        if not (self.object.has_prop("enchant-with") and self.object2.has_prop("enchanter")) :
            self.fail()
            return self.inform("enchant-with.failed")
        self.inform("enchant-with")
