# -*- coding: utf-8 -*-
#==============================================================================

from .event import Event2

class ConsumEvent(Event2):
    NAME = "consum"

    def perform(self):
        if not self.object.has_prop("consumable"):
            self.fail()
            return self.inform("consum.failed")
        self.object.move_to(None)
        self.inform("consum")
