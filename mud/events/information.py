from .event import Event1,Event2

class InformationEvent(Event1):
    NAME = "information"
    def perform(self):
        return self.actor.information()

class InformationOnEvent(Event2):
    NAME = "informationon"
    def perform(self):
        return self.inform("informationon")